﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class MessageBoard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        LabelTime.Text = DateTime.Now.ToString();
       
        DataClassesDataContext lq = new DataClassesDataContext();
        var mylq = from hs in lq.history select hs;
        GridviewHistory.DataSource = mylq;
        GridviewHistory.DataBind();
    }
      
    protected void TimerNow_Ontick(object sender, EventArgs e)
    {
        LabelTime.Text = DateTime.Now.ToString();
    }

    protected void ButtonSend_Click(object sender, EventArgs e)
    {
       // if (Session["CheckCode"].ToString() == yztext.Text)
        //{
            DataClassesDataContext lq = new DataClassesDataContext();
            history hy = new history();
            hy.username = Session["username"].ToString();
            hy.text = TextBoxChat.Text;
            hy.time = LabelTime.Text;
            hy.title = TextBoxTitle.Text;
            lq.history.InsertOnSubmit(hy);
            lq.SubmitChanges();
            Response.Redirect("MessageBoard.aspx");
       // }
       /* else
        {
            Response.Write("<script>alert('验证码错误');</script>");

        }

        */
       
    }

    protected void xgbt_Click(object sender, EventArgs e)
    {
        Response.Redirect("xiugai.aspx");
    }
}
